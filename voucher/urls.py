from django.urls import path
from .views import *

urlpatterns = [
    path('', voucher, name="voucher"),
    path('/formvoucher', formvoucher, name="formvoucher"),
    path('/updatevoucher', updatevoucher, name="updatevoucher"),
]