from django.shortcuts import render

# Create your views here.
content = {}


def voucher(request):
    return render(request, 'voucher.html', content)

def formvoucher(request):
    return render(request, 'formvoucher.html', content)

def updatevoucher(request):
    return render(request, 'updatevoucher.html', content)