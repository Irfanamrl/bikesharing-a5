from django.shortcuts import render

# Create your views here.
response = {'Author' : "Selvy Fitriani"}


def stasiun(request):
    return render(request, 'stasiun.html', response)

def daftar(request):
    return render(request, 'daftar_stasiun.html', response)

def update(request):
    return render(request, 'update_stasiun.html', response)
