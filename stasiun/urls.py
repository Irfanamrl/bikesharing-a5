from django.urls import path
from .views import *

urlpatterns = [
    path('', stasiun, name="stasiun"),
    path('daftar/', daftar, name="daftar"),
    path('update/', update, name="update"),
]
