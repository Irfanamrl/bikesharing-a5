from django.shortcuts import render

# Create your views here.
content = {}


def daftar_penugasan(request):
    return render(request, 'daftar_penugasan.html', content)
def register_penugasan(request):
    return render(request, 'register_penugasan.html', content)
def register_acara(request):
    return render(request, 'register_acara.html', content)
def daftar_acara(request):
    return render(request, 'daftar_acara.html', content)
def update_acara(request):
    return render(request, 'update_acara.html', content)
def update_penugasan(request):
    return render(request, 'update_penugasan.html', content)