from django.urls import path
from .views import *

urlpatterns = [
    path('daftar_penugasan/', daftar_penugasan, name='daftar_penugasan'),
    path('register_penugasan/', register_penugasan, name = 'register_penugasan'),
    path('register_acara/', register_acara, name = 'register_acara'),
    path('daftar_acara/', daftar_acara, name = 'daftar_acara'),
    path('update_acara/', update_acara, name = 'update_acara'),
    path('update_penugasan/', update_penugasan, name = 'update_penugasan'),
]