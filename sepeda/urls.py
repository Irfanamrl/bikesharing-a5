from django.urls import path
from .views import *

urlpatterns = [
    path('', sepeda, name="sepeda"),
    path('daftar/', daftar, name="daftar"),
    path('update/', update, name="update"),
]
