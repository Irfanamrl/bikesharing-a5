from django.shortcuts import render

# Create your views here.
response = {'Author' : "Selvy Fitriani"}


def sepeda(request):
    return render(request, 'sepeda.html', response)

def daftar(request):
    return render(request, 'daftar_sepeda.html', response)

def update(request):
    return render(request, 'update_sepeda.html', response)