from django.shortcuts import render

# Create your views here.
content = {}


def home(request):
    return render(request, 'homepage.html', content)

def login(request):
    return render(request, 'login.html', content)

def daftar(request):
    return render(request, 'daftar.html', content)

def topup(request):
    return render(request, 'topup.html', content)

def riwayat(request):
    return render(request, 'riwayat.html', content)

def laporan(request):
    return render(request, 'laporan.html', content)