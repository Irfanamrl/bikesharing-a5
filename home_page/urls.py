from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name="home_page"),
    path('login/',login, name="login"),
    path('daftar/', daftar, name="daftar"),
    path('topup/', topup, name="topup"),
    path('riwayat/', riwayat, name="riwayat"),
    path('laporan/', laporan, name="laporan")
]