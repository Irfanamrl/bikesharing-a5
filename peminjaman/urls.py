from django.urls import path
from .views import *

urlpatterns = [
    path('', peminjaman, name="peminjaman"),
    path('/formpeminjaman', formpeminjaman, name="formpeminjaman"),
    path('/updatepeminjaman', updatepeminjaman, name="updatepeminjaman"),
]