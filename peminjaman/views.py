from django.shortcuts import render

# Create your views here.
content = {}


def peminjaman(request):
    return render(request, 'peminjaman.html', content)

def formpeminjaman(request):
    return render(request, 'formpeminjaman.html', content)

def updatepeminjaman(request):
    return render(request, 'updatepeminjaman.html', content)